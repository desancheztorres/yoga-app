import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:yoga_app/detail_page.dart';

class Homepage extends StatefulWidget {
  const Homepage({Key key}) : super(key: key);

  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  int pageIndex = 0;

  @override
  Widget build(BuildContext context) {
    MediaQueryData _mediaQuery = MediaQuery.of(context);

    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            height: _mediaQuery.size.height / 7,
            decoration: BoxDecoration(color: Colors.white, boxShadow: [
              BoxShadow(
                color: Colors.blueGrey.withOpacity(0.3),
                blurRadius: 12,
                spreadRadius: 4,
              )
            ]),
            padding: EdgeInsets.only(bottom: 16.0, left: 24.0),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Text(
                "Yoga Secuences",
                style: TextStyle(
                  color: Colors.black87,
                  fontSize: 20,
                  letterSpacing: 1.5,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 24.0,
              left: 24.0,
            ),
            child: Text(
              "DISCOVER",
              style: TextStyle(
                color: Colors.black54,
                fontSize: 16.0,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.all(16.0),
            height: _mediaQuery.size.height / 1.52,
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => DetailPage(),
                        ),
                      );
                    },
                    child: Container(
                      margin: EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16.0),
                        color: Colors.blueGrey,
                        image: DecorationImage(
                          image: NetworkImage(
                              // https://cdn.pixabay.com/photo/2017/07/31/11/41/people-2557546_1280.jpg
                              "https://cdn.pixabay.com/photo/2017/07/31/11/41/people-2557547_960_720.jpg"),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(
                          left: 16.0,
                          top: 16.0,
                          bottom: 16.0,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.blueGrey[50],
                                      border: Border.all(
                                        color: Colors.black,
                                        width: 1,
                                      )),
                                  height: 32,
                                  width: 32,
                                  child: Center(
                                    child: Icon(
                                      Icons.whatshot,
                                      size: 16,
                                    ),
                                  ),
                                ),
                                SizedBox(width: 8.0),
                                Text(
                                  "HOT",
                                  style: TextStyle(
                                    fontSize: 12,
                                  ),
                                )
                              ],
                            ),
                            Spacer(),
                            Container(
                              height: 28,
                              width: 64,
                              decoration: BoxDecoration(
                                color: Colors.blueGrey[100],
                                borderRadius: BorderRadius.circular(
                                  24.0,
                                ),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Icon(
                                    Icons.av_timer,
                                    size: 14.0,
                                  ),
                                  SizedBox(width: 2.0),
                                  Text(
                                    "25 min",
                                    style: TextStyle(
                                      fontSize: 10.0,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 8.0),
                            Text(
                              "SUN SALUTATION",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                letterSpacing: 2.0,
                              ),
                            ),
                            SizedBox(height: 4.0),
                            Text("surya namaskar"),
                            SizedBox(height: 16.0),
                            Container(
                              height: 28.0,
                              width: 72.0,
                              decoration: BoxDecoration(
                                  color: Color(0xFF003847),
                                  borderRadius: BorderRadius.circular(4.0),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color(0xFF003847).withOpacity(0.2),
                                      blurRadius: 7,
                                      spreadRadius: 5,
                                    ),
                                  ]),
                              child: Center(
                                child: Text(
                                  "Start",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 10.0,
                                    letterSpacing: 2.0,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    margin: EdgeInsets.all(8.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(16.0),
                      color: Colors.blueGrey,
                      image: DecorationImage(
                        image: NetworkImage(
                            "https://cdn.pixabay.com/photo/2017/07/31/11/41/people-2557545_960_720.jpg"),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 16.0,
                        top: 16.0,
                        bottom: 16.0,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Container(
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.blueGrey[50],
                                    border: Border.all(
                                      color: Colors.black,
                                      width: 1,
                                    )),
                                height: 32,
                                width: 32,
                                child: Center(
                                  child: Icon(
                                    Icons.whatshot,
                                    size: 16,
                                  ),
                                ),
                              ),
                              SizedBox(width: 8.0),
                              Text(
                                "HOT",
                                style: TextStyle(
                                  fontSize: 12,
                                ),
                              )
                            ],
                          ),
                          Spacer(),
                          Container(
                            height: 28,
                            width: 64,
                            decoration: BoxDecoration(
                              color: Colors.blueGrey[100],
                              borderRadius: BorderRadius.circular(
                                24.0,
                              ),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.av_timer,
                                  size: 14.0,
                                ),
                                SizedBox(width: 2.0),
                                Text(
                                  "18 min",
                                  style: TextStyle(
                                    fontSize: 10.0,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 8.0),
                          Text(
                            "HEART OPENING",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              letterSpacing: 2.0,
                            ),
                          ),
                          SizedBox(height: 4.0),
                          Text("Heart chakra"),
                          SizedBox(height: 16.0),
                          Container(
                            height: 28.0,
                            width: 72.0,
                            decoration: BoxDecoration(
                                color: Color(0xFF003847),
                                borderRadius: BorderRadius.circular(4.0),
                                boxShadow: [
                                  BoxShadow(
                                    color: Color(0xFF003847).withOpacity(0.2),
                                    blurRadius: 7,
                                    spreadRadius: 5,
                                  ),
                                ]),
                            child: Center(
                              child: Text(
                                "Start",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 10.0,
                                  letterSpacing: 2.0,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }

  BottomNavigationBar _buildBottomNavigationBar() {
    return BottomNavigationBar(
      currentIndex: pageIndex,
      type: BottomNavigationBarType.fixed,
      elevation: 8,
      showSelectedLabels: true,
      selectedIconTheme: IconThemeData(color: Color(0xff003847)),
      selectedLabelStyle: TextStyle(color: Color(0xff003847)),
      unselectedIconTheme: IconThemeData(color: Colors.grey),
      onTap: (newIndex) {
        setState(() {
          pageIndex = newIndex;
        });
      },
      items: [
        BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text(
              "Home",
              style: TextStyle(color: Color(0xff003847)),
            )),
        BottomNavigationBarItem(
            icon: Icon(Icons.favorite_border), title: Text("Favourites")),
        BottomNavigationBarItem(icon: Icon(Icons.shop), title: Text("Shop")),
        BottomNavigationBarItem(
            icon: Icon(Icons.settings), title: Text("Settings")),
      ],
    );
  }
}
