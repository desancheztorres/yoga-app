import 'package:flutter/material.dart';
import 'package:yoga_app/homepage.dart';

void main() => runApp(YogaApp());

class YogaApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        accentColor: Color(0xff003847),
        bottomAppBarColor: Color(0xff003847),
        primarySwatch: Colors.blue,
      ),
      home: Homepage(),
    );
  }
}
